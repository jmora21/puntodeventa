DROP TABLE IF exists usuarios;

create table usuarios(
id_usuario bigint auto_increment primary key,
username varchar(25),
password varchar(25),
rol_usuario bigint,
fec_creacion date,
fec_ult_mod date
);