create table detalle_producto(
id_detalle_producto bigint auto_increment primary key,
producto bigint,
articulo bigint,
cantidad bigint,
user_creacion bigint,
user_mod bigint,
fec_creacion date,
fec_mod date
)