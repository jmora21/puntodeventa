create table proveedor(
id_proveedor bigint auto_increment primary key,
nombre_proveedor varchar(25),
numero_documento varchar(25),
telefono varchar(25),
fec_creacion date,
fec_mod date,
user_creacion bigint,
user_mod bigint
);