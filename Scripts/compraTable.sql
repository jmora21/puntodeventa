create table compra(
id_compra bigint auto_increment primary key,
articulo bigint,
cantidad bigint,
precio bigint,
comprobante_pago varchar(25),
numero_comprobante varchar(25),
tipo_ingreso bigint,
proveedor bigint,
fecha_compra date,
user_compra bigint
);