create table articulo(
id_articulo bigint auto_increment primary key,
nombre_articulo varchar(25),
medida bigint,
stock bigint,
user_creacion bigint,
user_mod bigint,
fec_creacion date,
fec_mod date
);