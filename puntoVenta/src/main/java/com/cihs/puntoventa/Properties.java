/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa;

/**
 *
 * @author Hamervit
 */
public class Properties {

    private static String UrlDB = "jdbc:mariadb://localhost:3306/puntoventa";
    private static String UserDB = "root";
    private static String PassDB = "1234";

    private static String username = "";
    private static long idUsername = 0;

    public static String getUrlDB() {
        return UrlDB;
    }

    public static void setUrlDB(String UrlDB) {
        Properties.UrlDB = UrlDB;
    }

    public static String getUserDB() {
        return UserDB;
    }

    public static void setUserDB(String UserDB) {
        Properties.UserDB = UserDB;
    }

    public static String getPassDB() {
        return PassDB;
    }

    public static void setPassDB(String PassDB) {
        Properties.PassDB = PassDB;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Properties.username = username;
    }

    public static long getIdUsername() {
        return idUsername;
    }

    public static void setIdUsername(long idUsername) {
        Properties.idUsername = idUsername;
    }

    

}
