/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.proveedorDAO;
import com.cihs.puntoventa.models.proveedor;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class proveedorService {
    public List<Map<String, Object>> getAll() {
        return new proveedorDAO().getAll();
    }

    public Map<String, Object> getById(proveedor entrada) {
        return new proveedorDAO().getUserById(entrada);
    }

    public boolean insertRegister(proveedor entrada) {
        return new proveedorDAO().insertRegister(entrada);
    }

    public boolean updateRegister(proveedor entrada) {
        return new proveedorDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(proveedor entrada) {
        return new proveedorDAO().deleteRegister(entrada);
    }
}
