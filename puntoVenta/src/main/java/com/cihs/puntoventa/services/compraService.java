/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.compraDAO;
import com.cihs.puntoventa.models.compra;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class compraService {
    public List<Map<String, Object>> getAll() {
        return new compraDAO().getAll();
    }

    public Map<String, Object> getById(compra entrada) {
        return new compraDAO().getUserById(entrada);
    }

    public boolean insertRegister(compra entrada) {
        return new compraDAO().insertRegister(entrada);
    }

    public boolean updateRegister(compra entrada) {
        return new compraDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(compra entrada) {
        return new compraDAO().deleteRegister(entrada);
    }
}
