/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.ventaDAO;
import com.cihs.puntoventa.models.venta;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class ventaService {
    public List<Map<String, Object>> getAll() {
        return new ventaDAO().getAll();
    }

    public Map<String, Object> getById(venta entrada) {
        return new ventaDAO().getUserById(entrada);
    }

    public boolean insertRegister(venta entrada) {
        return new ventaDAO().insertRegister(entrada);
    }

    public boolean updateRegister(venta entrada) {
        return new ventaDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(venta entrada) {
        return new ventaDAO().deleteRegister(entrada);
    }
}
