/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.productoDAO;
import com.cihs.puntoventa.models.producto;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class productoService {
    public List<Map<String, Object>> getAll() {
        return new productoDAO().getAll();
    }

    public Map<String, Object> getById(producto entrada) {
        return new productoDAO().getUserById(entrada);
    }

    public boolean insertRegister(producto entrada) {
        return new productoDAO().insertRegister(entrada);
    }

    public boolean updateRegister(producto entrada) {
        return new productoDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(producto entrada) {
        return new productoDAO().deleteRegister(entrada);
    }
}
