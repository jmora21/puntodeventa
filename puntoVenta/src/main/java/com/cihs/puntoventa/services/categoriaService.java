/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.categoriaDAO;
import com.cihs.puntoventa.models.categoria;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class categoriaService {
    public List<Map<String, Object>> getAll() {
        return new categoriaDAO().getAll();
    }

    public Map<String, Object> getById(categoria entrada) {
        return new categoriaDAO().getUserById(entrada);
    }

    public boolean insertRegister(categoria entrada) {
        return new categoriaDAO().insertRegister(entrada);
    }

    public boolean updateRegister(categoria entrada) {
        return new categoriaDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(categoria entrada) {
        return new categoriaDAO().deleteRegister(entrada);
    }
}
