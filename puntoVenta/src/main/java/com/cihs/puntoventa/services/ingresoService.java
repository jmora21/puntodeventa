/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.ingresoDAO;
import com.cihs.puntoventa.models.ingreso;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class ingresoService {
    
    public List<Map<String, Object>> getAll() {
        return new ingresoDAO().getAll();
    }

    public Map<String, Object> getById(ingreso entrada) {
        return new ingresoDAO().getUserById(entrada);
    }

    public boolean insertRegister(ingreso entrada) {
        return new ingresoDAO().insertRegister(entrada);
    }

    public boolean updateRegister(ingreso entrada) {
        return new ingresoDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(ingreso entrada) {
        return new ingresoDAO().deleteRegister(entrada);
    }
}
