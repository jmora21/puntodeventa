/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.detalleProductoDAO;
import com.cihs.puntoventa.models.detalleProducto;
import java.util.*;

/**
 *
 * @author Hamervit
 */
public class detalleProductoService {

    public List<Map<String, Object>> getAll() {
        return new detalleProductoDAO().getAll();
    }

    public Map<String, Object> getById(detalleProducto entrada) {
        return new detalleProductoDAO().getById(entrada);
    }

    public boolean insertRegister(detalleProducto entrada) {
        return new detalleProductoDAO().insertRegister(entrada);
    }

    public boolean updateRegister(detalleProducto entrada) {
        return new detalleProductoDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(detalleProducto entrada) {
        return new detalleProductoDAO().deleteRegister(entrada);
    }
}
