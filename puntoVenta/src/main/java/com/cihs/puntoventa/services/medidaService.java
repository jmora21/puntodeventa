/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.medidaDAO;
import com.cihs.puntoventa.models.medida;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class medidaService {

    public List<Map<String, Object>> getAll() {
        return new medidaDAO().getAll();
    }

    public Map<String, Object> getById(medida entrada) {
        return new medidaDAO().getUserById(entrada);
    }

    public boolean insertRegister(medida entrada) {
        return new medidaDAO().insertRegister(entrada);
    }

    public boolean updateRegister(medida entrada) {
        return new medidaDAO().updateRegister(entrada);
    }

    public boolean deleteRegister(medida entrada) {
        return new medidaDAO().deleteRegister(entrada);
    }
}
