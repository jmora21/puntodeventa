/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.articuloDAO;
import com.cihs.puntoventa.models.articulo;
import java.util.*;

/**
 *
 * @author Hamervit
 */
public class articuloService {
    
    public List<Map<String, Object>> getAll() {
        return new articuloDAO().getAll();
    }
    
    public Map<String, Object> getById(articulo entrada) {
        return new articuloDAO().getById(entrada);
    }
    
    public boolean insertRegister(articulo entrada) {
        return new articuloDAO().insertRegister(entrada);
    }
    
    public boolean updateRegister(articulo entrada) {
        return new articuloDAO().updateRegister(entrada);
    }
    
    public boolean deleteRegister(articulo entrada) {
        return new articuloDAO().deleteRegister(entrada);
    }
}
