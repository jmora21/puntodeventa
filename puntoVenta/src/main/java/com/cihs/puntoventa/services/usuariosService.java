/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.services;

import com.cihs.puntoventa.dao.usuariosDAO;
import com.cihs.puntoventa.models.usuarios;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hamervit
 */
public class usuariosService {
    
    public List<Map<String, Object>> getAll() {
        return new usuariosDAO().getAll();
    }
    
    public Map<String, Object> getById(usuarios user) {
        return new usuariosDAO().getUserById(user);
    }
    
    public boolean insertRegister(usuarios user) {
        return new usuariosDAO().insertUser(user);
    }
    
    public boolean updateRegister(usuarios user) {
        return new usuariosDAO().updateUser(user);
    }
    
    public boolean deleteRegister(usuarios user) {
        return new usuariosDAO().deleteUser(user);
    }
    
    public Map<String, Object> getUser(usuarios user) {
        return new usuariosDAO().getUser(user);
    }
}
