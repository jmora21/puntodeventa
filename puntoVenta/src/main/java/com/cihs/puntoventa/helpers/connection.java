/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.helpers;

import java.sql.*;
import com.cihs.puntoventa.Properties;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hamervit
 */
public class connection {

    private Connection con;

    public Connection getConnection() {
        try {
            String connectionString = Properties.getUrlDB();
            con = DriverManager.getConnection(connectionString, Properties.getUserDB(), Properties.getPassDB());
            return con;
        } catch (SQLException e) {
            return null;
        }
    }

    public List<Map<String, Object>> executeQueryForList(String query) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            List<Map<String, Object>> lm = new ArrayList<>();
            Map<String, Object> m;
            rs = sql.prepareStatement(query).executeQuery();
            while (rs.next()) {
                m = new LinkedHashMap<>();
                for (int x = 1; x <= rs.getMetaData().getColumnCount(); x++) {
                    m.put(rs.getMetaData().getColumnName(x), rs.getString(x));
                }
                lm.add(m);
            }
            sql.close();
            return lm;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Map<String, Object>> executeQueryForList(String query, Map<String, Object> variables) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            List<Map<String, Object>> lm = new ArrayList<>();
            Map<String, Object> m;
            PreparedStatement ps = sql.prepareStatement(query);
            int i = 1;
            for (Map.Entry<String, Object> var : variables.entrySet()) {
                ps.setString(i, String.valueOf(var.getValue()));
                i++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                m = new LinkedHashMap<>();
                for (int x = 1; x <= rs.getMetaData().getColumnCount(); x++) {
                    m.put(rs.getMetaData().getColumnName(x), rs.getString(x));
                }
                lm.add(m);
            }
            sql.close();
            return lm;
        } catch (Exception e) {
            return null;
        }
    }

    public Map<String, Object> executeQueryForMap(String query) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            Map<String, Object> m = new HashMap<>();
            rs = sql.prepareStatement(query).executeQuery();
            while (rs.next()) {
                for (int x = 1; x <= rs.getMetaData().getColumnCount(); x++) {
                    m.put(rs.getMetaData().getColumnName(x), rs.getString(x));
                }
                break;
            }
            sql.close();
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    public Map<String, Object> executeQueryForMap(String query, Map<String, Object> variables) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            Map<String, Object> m = new HashMap<>();
            PreparedStatement ps = sql.prepareStatement(query);
            int i = 1;
            for (Map.Entry<String, Object> var : variables.entrySet()) {
                ps.setString(i, String.valueOf(var.getValue()));
                i++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                for (int x = 1; x <= rs.getMetaData().getColumnCount(); x++) {
                    m.put(rs.getMetaData().getColumnName(x), rs.getString(x));
                }
                break;
            }
            sql.close();
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean execute(String query, Map<String, Object> variables) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            PreparedStatement ps = sql.prepareStatement(query);
            int i = 1;
            for (Map.Entry<String, Object> var : variables.entrySet()) {
                ps.setString(i, String.valueOf(var.getValue()));
                i++;
            }
            rs = ps.executeQuery();
            sql.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean execute(String query) {
        Connection sql = new connection().getConnection();
        ResultSet rs;
        try {
            sql.prepareStatement(query).execute();
            sql.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
