/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.detalleProducto;
import java.util.*;

/**
 *
 * @author Hamervit
 */
public class detalleProductoDAO {

    private connection con;

    public detalleProductoDAO() {
        con = new connection();
    }

    public List<Map<String, Object>> getAll() {
        String query = "SELECT id_detalle_producto, producto, articulo, cantidad, fec_creacion, fec_mod FROM detalle_Producto";
        return con.executeQueryForList(query);
    }

    public Map<String, Object> getById(detalleProducto entrada) {
        String query = "SELECT * FROM detalle_Producto WHERE id_detalle_producto = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idDetalleProducto", entrada.getIdDetalleProducto());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(detalleProducto entrada) {
        String query = "INSERT INTO detalle_Producto(producto, articulo, cantidad, user_creacion, fec_creacion, fec_mod) VALUES(?, ?, ?, 1, curdate(), null)";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("producto", entrada.getProducto());
        map.put("articulo", entrada.getArticulo());
        map.put("cantidad", entrada.getCantidad());
        //map.put("userCreacion", entrada.getUserCreacion());
        //map.put("userMod", entrada.getUserMod());
        //map.put("fecCreacion", entrada.getFecCreacion());
        //map.put("fecMod", entrada.getFecMod());
        return con.execute(query, map);
    }

    public boolean updateRegister(detalleProducto entrada) {
        String query = "UPDATE detalle_Producto SET producto = ?, articulo = ?, cantidad = ?, user_mod = 1, fec_mod = curdate() WHERE id_detalle_producto = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("producto", entrada.getProducto());
        map.put("articulo", entrada.getArticulo());
        map.put("cantidad", entrada.getCantidad());
//        map.put("userCreacion", entrada.getUserCreacion());
//        map.put("userMod", entrada.getUserMod());
//        map.put("fecCreacion", entrada.getFecCreacion());
//        map.put("fecMod", entrada.getFecMod());
        map.put("idDetalleProducto", entrada.getIdDetalleProducto());
        return con.execute(query, map);
    }

    public boolean deleteRegister(detalleProducto entrada) {
        String query = "DELETE FROM detalle_Producto WHERE id_detalle_producto = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idDetalleProducto", entrada.getIdDetalleProducto());
        return con.execute(query, map);
    }
}
