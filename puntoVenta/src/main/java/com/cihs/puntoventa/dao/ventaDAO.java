/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.venta;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class ventaDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM venta";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(venta entrada) {
        connection con = new connection();
        String query = "select * from venta where id_ventas = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idVenta", entrada.getIdVenta());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(venta entrada) {
        connection con = new connection();
        String query = "insert into venta(cantidad,precio_venta,producto,fecha_venta,user_venta)values(?,?,1,curdate(),1)";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("cantidad", entrada.getCantidad());
        map.put("precio_venta", entrada.getPrecioVenta());
        //map.put("producto", entrada.getProducto());
        //map.put("fecha_venta", entrada.getFechaVenta());
        //map.put("user_venta", entrada.getUserVenta());
        return con.execute(query, map);
    }

    public boolean updateRegister(venta entrada) {
        connection con = new connection();
        String query = "update venta set cantidad = ?, precio_venta = ?, producto = 1, fecha_venta = curdate(), user_venta = 1 where id_ventas = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("cantidad", entrada.getCantidad());
        map.put("precio_venta", entrada.getPrecioVenta());
        //map.put("producto", entrada.getProducto());
        //map.put("fecha_venta", entrada.getFechaVenta());
        //map.put("user_venta", entrada.getUserVenta());
        map.put("idVenta", entrada.getIdVenta());
        return con.execute(query, map);
    }

    public boolean deleteRegister(venta entrada) {
        connection con = new connection();
        String query = "DELETE FROM venta WHERE id_ventas = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idVenta", entrada.getIdVenta());
        return con.execute(query, map);
    }
}
