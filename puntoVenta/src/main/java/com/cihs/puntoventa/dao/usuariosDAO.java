/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.usuarios;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hamervit
 */
public class usuariosDAO {

    public List<Map<String, Object>> getAll() {
        String query = "SELECT id_usuario, username, rol_usuario, fec_creacion, fec_ult_mod FROM usuarios";
        connection con = new connection();
        return con.executeQueryForList(query);
    }

    public Map<String, Object> getUserById(usuarios user) {
        connection con = new connection();
        String query = "SELECT * FROM usuarios WHERE Id = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("Id", user.getIdUsuario());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertUser(usuarios user) {
        connection con = new connection();
        String query = "INSERT INTO usuarios(username, password, rol_usuario, fec_creacion) VALUES(?, ?, ?, curdate())";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("username", user.getUsername());
        map.put("password", user.getPassword());
        map.put("rolUser", user.getRolUsuario());
        return con.execute(query, map);
    }

    public boolean updateUser(usuarios user) {
        connection con = new connection();
        String query = "UPDATE usuarios SET username = ?, password = ?, rol_usuario = ?, fec_ult_mod = curdate() WHERE id_usuario = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("username", user.getUsername());
        map.put("password", user.getPassword());
        map.put("rolUser", user.getRolUsuario());
        map.put("Id", user.getIdUsuario());
        return con.execute(query, map);
    }

    public boolean deleteUser(usuarios user) {
        connection con = new connection();
        String query = "DELETE FROM usuarios WHERE id_usuario = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("Id", user.getIdUsuario());
        return con.execute(query, map);
    }

    public Map<String, Object> getUser(usuarios user) {
        connection con = new connection();
        String query = "SELECT id_usuario, Username, Password FROM usuarios WHERE username = ? AND password = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("username", user.getUsername());
        map.put("password", user.getPassword());
        return con.executeQueryForMap(query, map);
    }
}
