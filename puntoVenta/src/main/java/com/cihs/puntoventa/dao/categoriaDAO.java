/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.categoria;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class categoriaDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM categoria";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(categoria entrada) {
        connection con = new connection();
        String query = "select * from categoria where id_categoria = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idCategoria", entrada.getIdCategoria());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(categoria entrada) {
        connection con = new connection();
        String query = "insert into categoria(nombre_categoria, user_creacion, fec_creacion)values(?,1,curdate())";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreCategoria", entrada.getNombreCategoria());
        //map.put("userCrea", entrada.getUserCreacion());
        //map.put("fecCrea", entrada.getFecCreacion());
        return con.execute(query, map);
    }

    public boolean updateRegister(categoria entrada) {
        connection con = new connection();
        String query = "update categoria set nombre_categoria = ?, user_mod = 1, fec_mod = curdate() where id_categoria = ? ";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreCategoria", entrada.getNombreCategoria());
        //map.put("userMod", entrada.getUserMod());
        //map.put("fecMod", entrada.getFecMod());
        map.put("idCategoria", entrada.getIdCategoria());
        return con.execute(query, map);
    }

    public boolean deleteRegister(categoria entrada) {
        connection con = new connection();
        String query = "DELETE FROM categoria WHERE id_categoria = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idCategoria", entrada.getIdCategoria());
        return con.execute(query, map);
    }
}
