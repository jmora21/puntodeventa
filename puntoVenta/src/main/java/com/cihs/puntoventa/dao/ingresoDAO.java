/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.ingreso;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class ingresoDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM ingreso";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(ingreso entrada) {
        connection con = new connection();
        String query = "select * from ingreso where id_ingreso = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idIngreso", entrada.getIdIngreso());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(ingreso entrada) {
        connection con = new connection();
        String query = "insert into ingreso(tipo_ingreso, user_creacion,fec_creacion)values(?, 1, curdate())";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("tipoIngreso", entrada.getTipoIngreso());
        //map.put("userCrea", entrada.getUserCreacion());
        //map.put("fecCrea", entrada.getFecCreacion());
        return con.execute(query, map);
    }

    public boolean updateRegister(ingreso entrada) {
        connection con = new connection();
        String query = "update ingreso set tipo_ingreso = ?, user_mod = 1, fec_mod = curdate() where id_ingreso = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("tipoIngreso", entrada.getTipoIngreso());
        //map.put("userMod", entrada.getUserMod());
        //map.put("fecMod", entrada.getFecMod());
        map.put("idIngreso", entrada.getIdIngreso());
        return con.execute(query, map);
    }

    public boolean deleteRegister(ingreso entrada) {
        connection con = new connection();
        String query = "DELETE FROM ingreso WHERE id_ingreso = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idIngreso", entrada.getIdIngreso());
        return con.execute(query, map);
    }
}
