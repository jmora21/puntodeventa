/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.compra;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class compraDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM compra";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(compra entrada) {
        connection con = new connection();
        String query = "select * from compra where id_compra = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idCompra", entrada.getIdCompra());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(compra entrada) {
        connection con = new connection();
        String query = "insert into compra(articulo,cantidad,precio,comprobante_pago,numero_comprobante,tipo_ingreso,proveedor,fecha_compra,user_compra)values(1, 1, 1, 1, 1, 1, 1,curdate(),1)";
        Map<String, Object> map = new LinkedHashMap<>();
        //map.put("articulo", entrada.getArticulo());
        //map.put("cantidad", entrada.getCantidad());
        //map.put("precio", entrada.getPrecio());
        //map.put("comprobantePago", entrada.getComprobantePago());
        //map.put("numeroComprobante", entrada.getNumeroComprabante());
        //map.put("tipoIngreso", entrada.getTipoIngreso());
        //map.put("proveedor", entrada.getProveedor());
        //map.put("fechaCompra", entrada.getFechaCompra());
        //map.put("userCompra", entrada.getUserCompra());
        return con.execute(query, map);
    }

    public boolean updateRegister(compra entrada) {
        connection con = new connection();
        String query = "update compra set articulo = ?, cantidad = ?, precio = ?, comprobante_pago = ?, numero_comprobante = ?, tipo_ingreso = ?, proveedor = ?, fecha_compra = curdate(), user_compra = 1 where id_compra = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("articulo", entrada.getArticulo());
        map.put("cantidad", entrada.getCantidad());
        map.put("precio", entrada.getPrecio());
        map.put("comprobantePago", entrada.getComprobantePago());
        map.put("numeroComprobante", entrada.getNumeroComprabante());
        map.put("tipoIngreso", entrada.getTipoIngreso());
        map.put("proveedor", entrada.getProveedor());
        //map.put("fechaCompra", entrada.getFechaCompra());
        //map.put("userCompra", entrada.getUserCompra());
        map.put("idCompra", entrada.getIdCompra());
        return con.execute(query, map);
    }

    public boolean deleteRegister(compra entrada) {
        connection con = new connection();
        String query = "DELETE FROM compra WHERE id_compra = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idCompra", entrada.getIdCompra());
        return con.execute(query, map);
    }
}
