/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.producto;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class productoDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM producto";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(producto entrada) {
        connection con = new connection();
        String query = "select * from producto where id_producto = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idProducto", entrada.getIdProducto());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(producto entrada) {
        connection con = new connection();
        String query = "insert into producto(nombre_producto,precio_venta,categoria,user_creacion,fec_creacion)values(?,?,1,1,curdate())";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreProducto", entrada.getNombreProducto());
        map.put("precioVenta", entrada.getPrecioVenta());
        //map.put("categoria", entrada.getCategoria());
        //map.put("userCreacion", entrada.getUserCreacion());
        //map.put("fecCreacion", entrada.getFecCreacion());
        return con.execute(query, map);
    }

    public boolean updateRegister(producto entrada) {
        connection con = new connection();
        String query = "update producto set nombre_producto = ?, precio_venta = ?, categoria = 1, user_mod = 1, fec_mod = curdate() where id_producto = ? ";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreProducto", entrada.getNombreProducto());
        map.put("precioVenta", entrada.getPrecioVenta());
        //map.put("categoria", entrada.getCategoria());
        //map.put("userMod", entrada.getUserMod());
        //map.put("fecMod", entrada.getFecMod());
        map.put("idProducto", entrada.getIdProducto());
        return con.execute(query, map);
    }

    public boolean deleteRegister(producto entrada) {
        connection con = new connection();
        String query = "DELETE FROM producto WHERE id_producto = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idProducto", entrada.getIdProducto());
        return con.execute(query, map);
    }
}
