/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.proveedor;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class proveedorDAO {

    public List<Map<String, Object>> getAll() {
        String query = "SELECT id_proveedor, nombre_proveedor, numero_documento,"
                + " telefono FROM proveedor";
        connection con = new connection();
        return con.executeQueryForList(query);
    }

    public Map<String, Object> getUserById(proveedor entrada) {
        connection con = new connection();
        String query = "select * from proveedor where id_proveedor = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idProveedor", entrada.getIdProveedor());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(proveedor entrada) {
        connection con = new connection();
        String query = "insert into proveedor(nombre_proveedor, numero_documento, telefono, fec_creacion, "
                + "user_creacion)values(?,?,?, curdate(),1)";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreProveedor", entrada.getNombreProveedor());
        map.put("numeroDocumento", entrada.getNumeroDocumento());
        map.put("telefono", entrada.getTelefono());
        // map.put("userCreacion", entrada.getUserCreacion());
        return con.execute(query, map);
    }

    public boolean updateRegister(proveedor entrada) {
        connection con = new connection();
        String query = "update proveedor set nombre_proveedor = ?, numero_documento = ?, telefono = ?, fec_mod = curdate(), user_mod = 1 where id_proveedor = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreProveedor", entrada.getNombreProveedor());
        map.put("numeroDocumento", entrada.getNumeroDocumento());
        map.put("telefono", entrada.getTelefono());
       // map.put("fecMod", entrada.getFecMod());
       // map.put("userMod", entrada.getUserMod());
        map.put("idProveedor", entrada.getIdProveedor());
        return con.execute(query, map);
    }

    public boolean deleteRegister(proveedor entrada) {
        connection con = new connection();
        String query = "DELETE FROM proveedor WHERE id_proveedor = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idProveedor", entrada.getIdProveedor());
        return con.execute(query, map);
    }
}
