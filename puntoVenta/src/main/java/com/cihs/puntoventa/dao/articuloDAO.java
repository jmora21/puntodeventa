/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.articulo;
import java.util.*;

/**
 *
 * @author Hamervit
 */
public class articuloDAO {

    private connection con;

    public articuloDAO() {
        con = new connection();
    }

    public List<Map<String, Object>> getAll() {
        String query = "SELECT id_articulo, nombre_articulo, medida, stock, fec_creacion, fec_mod FROM articulo";
        return con.executeQueryForList(query);
    }

    public Map<String, Object> getById(articulo entrada) {
        String query = "SELECT * FROM articulo WHERE id_articulo = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idArticulo", entrada.getIdArticulo());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(articulo entrada) {
        String query = "INSERT INTO articulo(nombre_articulo, medida, stock, user_creacion, user_mod, fec_creacion) VALUES(?, 1, 0, 1, null, curdate())";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombreArticulo", entrada.getNombreArticulo());
        //map.put("medida", entrada.getMedida());
        //map.put("stock", entrada.getStock());
        //map.put("userCreacion", entrada.getUserCreacion());
//        map.put("userMod", entrada.getUserMod());
//        map.put("fecCreacion", entrada.getFecCreacion());
//        map.put("fecMod", entrada.getFecMod());
        return con.execute(query, map);
    }

    public boolean updateRegister(articulo entrada) {
        String query = "UPDATE articulo SET nombre_articulo = ?, medida = 1, stock = 0, user_creacion = 1, user_mod = 1, fec_mod = curdate() WHERE id_articulo = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("nombre_articulo", entrada.getNombreArticulo());
//        map.put("medida", entrada.getMedida());
//        map.put("stock", entrada.getStock());
//        map.put("userCreacion", entrada.getUserCreacion());
//        map.put("userMod", entrada.getUserMod());
//        map.put("fecCreacion", entrada.getFecCreacion());
//        map.put("fecMod", entrada.getFecMod());
        map.put("idArticulo", entrada.getIdArticulo());
        return con.execute(query, map);
    }

    public boolean deleteRegister(articulo entrada) {
        String query = "DELETE FROM articulo WHERE id_articulo = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idArticulo", entrada.getIdArticulo());
        return con.execute(query, map);
    }
}
