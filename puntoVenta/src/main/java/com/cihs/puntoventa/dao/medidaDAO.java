/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.dao;

import com.cihs.puntoventa.helpers.connection;
import com.cihs.puntoventa.models.medida;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tecnoevolucion
 */
public class medidaDAO {
    
    public List<Map<String, Object>> getAll() {
        String query = "SELECT * FROM medida";
        connection con = new connection();
        return con.executeQueryForList(query);
    }
    
    public Map<String, Object> getUserById(medida entrada) {
        connection con = new connection();
        String query = "select * from medida where id_medida = ?";
        Map<String, Object> map = new HashMap<>();
        map.put("idMedida", entrada.getIdMedida());
        return con.executeQueryForMap(query, map);
    }

    public boolean insertRegister(medida entrada) {
        connection con = new connection();
        String query = "insert into medida(tipo_medida,siglas)values(?,?)";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("tipoMedida", entrada.getTipoMedida());
        map.put("siglas", entrada.getSiglas());
        return con.execute(query, map);
    }

    public boolean updateRegister(medida entrada) {
        connection con = new connection();
        String query = "update medida set tipo_medida = ?, siglas = ? where id_medida = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("tipoMedida", entrada.getTipoMedida());
        map.put("siglas", entrada.getSiglas());
        map.put("idMedida", entrada.getIdMedida());
        return con.execute(query, map);
    }

    public boolean deleteRegister(medida entrada) {
        connection con = new connection();
        String query = "DELETE FROM medida WHERE id_medida = ?";
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("idMedida", entrada.getIdMedida());
        return con.execute(query, map);
    }
}
