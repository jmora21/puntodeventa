/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Hamervit
 */
public class articulo {

    private long idArticulo;
    private String nombreArticulo;
    private double medida;
    private double stock;
    private double userCreacion;
    private double userMod;
    private String fecCreacion;
    private String fecMod;

    public long getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public double getMedida() {
        return medida;
    }

    public void setMedida(double medida) {
        this.medida = medida;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public double getUserCreacion() {
        return userCreacion;
    }

    public void setUserCreacion(double userCreacion) {
        this.userCreacion = userCreacion;
    }

    public double getUserMod() {
        return userMod;
    }

    public void setUserMod(double userMod) {
        this.userMod = userMod;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecMod() {
        return fecMod;
    }

    public void setFecMod(String fecMod) {
        this.fecMod = fecMod;
    }

}
