/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author isaac
 */
public class producto {

    private long idProducto;
    private String nombreProducto;
    private double precioVenta;
    private double categoria;
    private double userCreacion;
    private double userMod;
    private String fecCreacion;
    private String fecMod;

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getCategoria() {
        return categoria;
    }

    public void setCategoria(double categoria) {
        this.categoria = categoria;
    }

    public double getUserCreacion() {
        return userCreacion;
    }

    public void setUserCreacion(double userCreacion) {
        this.userCreacion = userCreacion;
    }

    public double getUserMod() {
        return userMod;
    }

    public void setUserMod(double userMod) {
        this.userMod = userMod;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecMod() {
        return fecMod;
    }

    public void setFecMod(String fecMod) {
        this.fecMod = fecMod;
    }
}
