/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Cristian Contreras
 */
public class proveedor {

    private long idProveedor;
    private String nombreProveedor;
    private String numeroDocumento;
    private String telefono;
    private String fecCreacion;
    private String fecMod;
    private double userCreacion;
    private double userMod;

    public long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecMod() {
        return fecMod;
    }

    public void setFecMod(String fecMod) {
        this.fecMod = fecMod;
    }

    public double getUserCreacion() {
        return userCreacion;
    }

    public void setUserCreacion(double userCreacion) {
        this.userCreacion = userCreacion;
    }

    public double getUserMod() {
        return userMod;
    }

    public void setUserMod(double userMod) {
        this.userMod = userMod;
    }

}
