/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Hamervit
 */
public class detalleProducto {

    private long idDetalleProducto;
    private double producto;
    private double articulo;
    private double cantidad;
    private double userCreacion;
    private double userMod;
    private String fecCreacion;
    private String fecMod;

    public long getIdDetalleProducto() {
        return idDetalleProducto;
    }

    public void setIdDetalleProducto(long idDetalleProducto) {
        this.idDetalleProducto = idDetalleProducto;
    }

    public double getProducto() {
        return producto;
    }

    public void setProducto(double producto) {
        this.producto = producto;
    }

    public double getArticulo() {
        return articulo;
    }

    public void setArticulo(double articulo) {
        this.articulo = articulo;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getUserCreacion() {
        return userCreacion;
    }

    public void setUserCreacion(double userCreacion) {
        this.userCreacion = userCreacion;
    }

    public double getUserMod() {
        return userMod;
    }

    public void setUserMod(double userMod) {
        this.userMod = userMod;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecMod() {
        return fecMod;
    }

    public void setFecMod(String fecMod) {
        this.fecMod = fecMod;
    }

}
