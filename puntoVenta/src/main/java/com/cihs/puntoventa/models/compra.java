/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Cristian Contreras
 */
public class compra {

    private long idCompra;
    private double articulo;
    private double cantidad;
    private double precio;
    private double comprobantePago;
    private double numeroComprabante;
    private double tipoIngreso;
    private double proveedor;
    private String fechaCompra;
    private double userCompra;

    public long getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(long idCompra) {
        this.idCompra = idCompra;
    }

    public double getArticulo() {
        return articulo;
    }

    public void setArticulo(double articulo) {
        this.articulo = articulo;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getComprobantePago() {
        return comprobantePago;
    }

    public void setComprobantePago(double comprobantePago) {
        this.comprobantePago = comprobantePago;
    }

    public double getNumeroComprabante() {
        return numeroComprabante;
    }

    public void setNumeroComprabante(double numeroComprabante) {
        this.numeroComprabante = numeroComprabante;
    }

    public double getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(double tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public double getProveedor() {
        return proveedor;
    }

    public void setProveedor(double proveedor) {
        this.proveedor = proveedor;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public double getUserCompra() {
        return userCompra;
    }

    public void setUserCompra(double userCompra) {
        this.userCompra = userCompra;
    }

}
