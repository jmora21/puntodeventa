/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Cristian Contreras
 */
public class ingreso {

    private long idIngreso;
    private String tipoIngreso;
    private double userCreacion;
    private double userMod;
    private String fecCreacion;
    private String fecMod;

    public long getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(long idIngreso) {
        this.idIngreso = idIngreso;
    }

    public String getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(String tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public double getUserCreacion() {
        return userCreacion;
    }

    public void setUserCreacion(double userCreacion) {
        this.userCreacion = userCreacion;
    }

    public double getUserMod() {
        return userMod;
    }

    public void setUserMod(double userMod) {
        this.userMod = userMod;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecMod() {
        return fecMod;
    }

    public void setFecMod(String fecMod) {
        this.fecMod = fecMod;
    }

}
