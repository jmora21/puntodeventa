/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.models;

/**
 *
 * @author Hamervit
 */
public class usuarios {

    private long idUsuario;
    private String username;
    private String password;
    private double rolUsuario;
    private String fecCreacion;
    private String fecUltMod;

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(double rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public String getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(String fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public String getFecUltMod() {
        return fecUltMod;
    }

    public void setFecUltMod(String fecUltMod) {
        this.fecUltMod = fecUltMod;
    }

}
