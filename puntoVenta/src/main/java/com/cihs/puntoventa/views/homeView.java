/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cihs.puntoventa.views;

import com.cihs.puntoventa.views.Internal.categoriaInternalView;
import com.cihs.puntoventa.views.Internal.compraInternalView;
import com.cihs.puntoventa.views.Internal.ingresoInternalView;
import com.cihs.puntoventa.views.Internal.productoInternalView;
import com.cihs.puntoventa.views.Internal.proveedorInternalView;
import com.cihs.puntoventa.views.Internal.ventaInternalView;

/**
 *
 * @author Cristian Contreras
 */
public class homeView extends javax.swing.JFrame {

    /**
     * Creates new form homeView
     */
    public homeView() {
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktop = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        articuloMenu = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        producto = new javax.swing.JMenu();
        productoMenu = new javax.swing.JMenuItem();
        compraMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        proveedor = new javax.swing.JMenu();
        proveedorMenu = new javax.swing.JMenuItem();
        ingreso = new javax.swing.JMenu();
        ingresoMenu = new javax.swing.JMenuItem();
        venta = new javax.swing.JMenu();
        menuVenta = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        menuCategoria = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        desktop.setBackground(new java.awt.Color(102, 255, 255));

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 704, Short.MAX_VALUE)
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 315, Short.MAX_VALUE)
        );

        articuloMenu.setText("Articulo");
        articuloMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                articuloMenuActionPerformed(evt);
            }
        });
        jMenuBar1.add(articuloMenu);

        jMenu5.setText("Detalle");
        jMenuBar1.add(jMenu5);

        producto.setText("Producto");

        productoMenu.setText("Menu producto");
        productoMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                productoMenuActionPerformed(evt);
            }
        });
        producto.add(productoMenu);

        jMenuBar1.add(producto);

        compraMenu.setText("Compra");
        compraMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compraMenuActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Menu compra");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        compraMenu.add(jMenuItem1);

        jMenuBar1.add(compraMenu);

        proveedor.setText("Proveedor");
        proveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proveedorActionPerformed(evt);
            }
        });

        proveedorMenu.setText("Menu proveedor");
        proveedorMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proveedorMenuActionPerformed(evt);
            }
        });
        proveedor.add(proveedorMenu);

        jMenuBar1.add(proveedor);

        ingreso.setText("Ingreso");
        ingreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingresoActionPerformed(evt);
            }
        });

        ingresoMenu.setText("Menu ingreso");
        ingresoMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingresoMenuActionPerformed(evt);
            }
        });
        ingreso.add(ingresoMenu);

        jMenuBar1.add(ingreso);

        venta.setText("Ventas");

        menuVenta.setText("Menu ventas");
        menuVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuVentaActionPerformed(evt);
            }
        });
        venta.add(menuVenta);

        jMenuBar1.add(venta);

        jMenu8.setText("Administración");
        jMenuBar1.add(jMenu8);

        jMenu7.setText("Reportes");
        jMenuBar1.add(jMenu7);

        jMenu1.setText("Categoria");

        menuCategoria.setText("Menu categoria");
        menuCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCategoriaActionPerformed(evt);
            }
        });
        jMenu1.add(menuCategoria);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(desktop)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(desktop)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void articuloMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_articuloMenuActionPerformed
        
    }//GEN-LAST:event_articuloMenuActionPerformed

    private void compraMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compraMenuActionPerformed
        compraInternalView compra = new compraInternalView();
        desktop.add(compra);
        compra.show();
    }//GEN-LAST:event_compraMenuActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        compraInternalView compra = new compraInternalView();
        desktop.add(compra);
        compra.show();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void proveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proveedorActionPerformed
        
    }//GEN-LAST:event_proveedorActionPerformed

    private void proveedorMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proveedorMenuActionPerformed
      proveedorInternalView proveedor = new proveedorInternalView();
      desktop.add(proveedor);
      proveedor.show();
    }//GEN-LAST:event_proveedorMenuActionPerformed

    private void ingresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingresoActionPerformed
       
    }//GEN-LAST:event_ingresoActionPerformed

    private void ingresoMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingresoMenuActionPerformed
        ingresoInternalView ingreso = new ingresoInternalView();
        desktop.add(ingreso);
        ingreso.show();
    }//GEN-LAST:event_ingresoMenuActionPerformed

    private void productoMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_productoMenuActionPerformed
       productoInternalView producto = new productoInternalView();
       desktop.add(producto);
       producto.show();
    }//GEN-LAST:event_productoMenuActionPerformed

    private void menuVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuVentaActionPerformed
        ventaInternalView venta = new ventaInternalView();
        desktop.add(venta);
        venta.show();
    }//GEN-LAST:event_menuVentaActionPerformed

    private void menuCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCategoriaActionPerformed
        categoriaInternalView categoria = new categoriaInternalView();
        desktop.add(categoria);
        categoria.show();
    }//GEN-LAST:event_menuCategoriaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(homeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(homeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(homeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(homeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new homeView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu articuloMenu;
    private javax.swing.JMenu compraMenu;
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JMenu ingreso;
    private javax.swing.JMenuItem ingresoMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem menuCategoria;
    private javax.swing.JMenuItem menuVenta;
    private javax.swing.JMenu producto;
    private javax.swing.JMenuItem productoMenu;
    private javax.swing.JMenu proveedor;
    private javax.swing.JMenuItem proveedorMenu;
    private javax.swing.JMenu venta;
    // End of variables declaration//GEN-END:variables
}
